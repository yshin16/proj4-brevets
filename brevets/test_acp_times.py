"""
Nose tests for acp_times.py
"""
import acp_times

import nose
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)


def basic_open_test():
    """
    basic open time test
    """
    info = acp_times.open_time(100,"200", "2017-01-01T00:00:00+00:00")

    assert str(info) == "2017-01-01T02:56:00+00:00"


def basic_close_test():
    """
    basic close time test
    """
    info = acp_times.close_time(100,"200", "2017-01-01T00:00:00+00:00")

    assert str(info) == "2017-01-01T06:40:00+00:00"


def overdistance_open_test():
    """
    When control distance > total distance, checking open time
    """
    info = acp_times.open_time(300,"200", "2017-01-01T00:00:00+00:00")

    assert str(info) == "2017-01-01T08:49:00+00:00"


def overdistance_close_test():
    """
    When control distance > total distance, checking close time
    """
    info = acp_times.close_time(300,"200", "2017-01-01T00:00:00+00:00")

    assert str(info) == "2017-01-01T20:00:00+00:00"


def diff_totaldistance_test():
    """
    when total distance is not set on default(200km)
    """
    info = acp_times.open_time(500,"1000", "2017-01-01T00:00:00+00:00")

    assert str(info) == "2017-01-01T19:14:00+00:00"
